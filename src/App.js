import React, { Component } from 'react';
import convert from './convert';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      input: ''
    };
  }
  handleChange(event) {
    this.setState({
      input: event.target.value
    });
  }
  render() {
    let input = this.state.input;
    return (
      <div className="container">
        <h1>FizzBuzz Pro</h1>
        <form onSubmit={(e) => e.preventDefault()}>
          <div className="form-group">
            <label className="control-label" htmlFor="input">Input</label>
            <input type="text" className="form-control" id="input" value={input} onChange={this.handleChange.bind(this)}/>
          </div>
          <div className="form-group">
            <label className="control-label" htmlFor="output">Output</label>
            <p className="form-control-static">
              {convert(input)}
            </p>
          </div>
        </form>
      </div>
    );
  }
}

export default App;
