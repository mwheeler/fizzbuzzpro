function isLetter(char) {
  return Boolean(char.match(/[A-Za-z]/));
}

function switchCase(char) {
  if (char === char.toUpperCase()) {
    return char.toLowerCase();
  }
  return char.toUpperCase();
}

export default function convert(input) {
  return input.split('').reduce((prev, curr) => {
    if (!isLetter(curr)) {
      return prev + curr;
    }
    let count = prev.split('').filter(isLetter).length;
    if (count % 5 === 4) {
      return prev + switchCase(curr);
    }
    return prev + curr;
  }, '');
}
