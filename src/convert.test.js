import convert from './convert';

it('handles the given example', () => {
  const input = "Hi, my name is Bob.";
  const output = "Hi, my Name iS Bob.";
  expect(convert(input)).toEqual(output);
});
 
it('switches the case of every 5th letter', () => {
  const input  = "Back in the U.S.A. by Chuck Berry.";
  const output = "Back In the u.S.A. by chuck berry.";
  expect(convert(input)).toEqual(output);
});

it('also works for Beatles tunes', () => {
  const input  = "Back in the USSR by the Beatles.";
  const output = "Back In the uSSR bY the BEatleS.";
  expect(convert(input)).toEqual(output);
});
